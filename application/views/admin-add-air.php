<!DOCTYPE html>
<html>
  <?php
	 include"includes/admin_header.php";
	 ?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Left side column. contains the logo and sidebar -->
     <?php
	 include"includes/admin_sidebar.php";
	 ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class="add_promocode">
          Adicionar transferência de aeroporto
          </h1>
		  
		 
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">ABD</a></li>
            <li class="active">Transferência de aeroporto</li>
          </ol>
        </section>
		   <?php
	   $query = $this->db->query(" SELECT * FROM `settings` order by id DESC ");
		$row = $query->row('settings');
	   if($row->places =='google'){	
	  ?>
	<script src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false" ></script>
         
       
		  
		   
        <script type="text/javascript">
		
function initialize(id) {
var cntry = document.getElementById('countryin').value;
var options = {
types: ['(cities)'],
  componentRestrictions: {country: cntry}
 };

 var input = document.getElementById(id);
 var autocomplete = new google.maps.places.Autocomplete(input, options);
  }
   google.maps.event.addDomListener(window, 'load', initialize);
            

        </script>
		
	<?php
	   }else{
	   ?>  <script type="text/javascript">
      $(document).ready(function(){
          $(".auto-place").autocomplete({
	      source:'<?php echo base_url();?>admin/auto_places',
           minLength:1
          });
        });
        </script>

    <?php
	   }
	?>
  <input type='hidden' value='<?php echo $row->country;?>' id='countryin'>
	
        <!-- Main content -->
 <div class="">
			   <div class="">
                <div class="col-lg-6">
           <div class="box box-primary edit_promoform1">
				 
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
				   <div class="adminuser1" tabindex='1'></div>
                </div><!-- /.box-header -->
                <!-- form start -->
				
              <form role="form"  method="post" id="user_reg">
				
                  <div class="box-body">
				  <div class="form-group">
                                            <label>Usuário</label>
                                           <select id="signup-username" class="form-control regcom select2"  style="width: 100%;" name="username">
						                    <option value='0'>Selecione um usuário</option>
											<?php
											$query1 = $this->db->query("SELECT * FROM  userdetails");
							             foreach($query1->result_array('userdetails') as $row1){
												?><option value="<?php  echo $row1['username'];?>" ><?php  echo $row1['username']; ?></option>
												<?php
											 }
											 ?>
											</select>
											</div>
				  
						
											<div class="form-group">
                                            <label>Type</label>
                                            <select  class="form-control regcom select2"  style="width: 100%;" name='transfer'id='transfer'>
						                    <option value='going'>Indo para o aeroporto</option> 
											<option value='coming'>Vindo do aeroporto</option> </select>
											</div>
                                       
									   <div class="form-group">
                                            <label>Aeroporto</label>
                                            <select  class="form-control regcom select2"  style="width: 100%;" name='transfer1' id="transfer1">
						                     <?php
								 $query1 = $this->db->query("SELECT * FROM airport_details ");
								  foreach($query1->result_array('airport_details') as $row1){
								?>
                                          <option value="<?php echo $row1['name'];?>"><?php echo $row1['name'];?></option>
                                          
                                          <?php
								  }
								  ?></select>
											</div>
                                        <div class="form-group" id='pick'>
                                            <label>Area de partida</label>
                                           <input class="form-control regcom sample" placeholder="Partida" name='pickup_area'id="autocomplete" autocomplete="on" onClick="initialize(this.id);">
                                        </div>
                                         <div class="form-group" id='drop'>
                                            <label>Area de destino</label>
                                           <input class="form-control regcom sample" placeholder="Destino" name='drop_area' id="autocomp" autocomplete="on" onClick="initialize(this.id);">
                                        </div>
                                        
										
										 <div class="form-group">
                    <label>Data de partida</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right regcom date" id="datepickerp" readonly name='pickup_date'>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                                        
                                        
										
										 <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label>Hora de partida</label>
                      <div class="input-group">
					  <div class="time_div">
                        <input type="text" class="form-control timepicker regcom times" placeholder="Hora de partida" name="pickup_time">
						</div>
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div><!-- /.input group -->
                    </div><!-- /.form group -->
                  </div>
										
										
										 <div class="form-group">
                                       
                                        <input type="button" class="btn btn-primary" value="Salvar"  name="Save" id="useradd">
                                        <button type="reset" class="btn btn-primary">Limpar </button>
                                        </div>
				  
				</div>  
				  
	</div>
				 
                
              </div><!-- /.box -->
			  
			  
			  
			  
			  <div class="col-lg-6">
			             <div class="box box-primary edit_promoform">
				 
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <!-- form start -->

				
                  <div class="box-body">
				  
					                   <div class="form-group ">
										  
												   
                                            <label>Selecione um carro</label>
											<select id="car" class="form-control select2"  style="width: 100%;"  name="taxi_type">
						                    <option value='0'>Selecione um carro</option>
											<?php
											$query1 = $this->db->query("SELECT * FROM  cabdetails WHERE transfertype='Point to Point Transfer'");
							             foreach($query1->result_array('cabdetails') as $row1){
												?><option value="<?php  echo $row1['cartype'];?>" ><?php  echo $row1['cartype']; ?></option>
												<?php
											 }
											 ?>
											</select>
                                        </div>
										 
                                        <div class="form-group">
                                            <label>Area</label>
                                            <input class="form-control regcom sample" placeholder="Area" name="area" id="area1">
                                        </div>
										  <div class="form-group">
                                            <label>Número do Vôo</label>
                                            <input class="form-control regcom sample" placeholder="Número do vôo" name="flight_number" id="flight_number">
                                        </div>
										<div class="form-group">
                                            <label>Endereço de partida</label>
                                            <input class="form-control regcom sample" placeholder="Endereço" name="pickup_address" id="pickupadd">
                                        </div>
										<div class="form-group">
                                            <label>Atribuído para</label>
                                            <select id="assigned" class="form-control regcom select2"  style="width: 100%;" name="assigned_for" id='aaddress'>
						                    <option value='0'>Selecione o motorista</option>
                                            <?php
											$query1 = $this->db->query("SELECT driver_details.* FROM driver_details where NOT EXISTS(select * from bookingdetails where driver_details.id=bookingdetails.assigned_for and bookingdetails.status='Processing') ");
											 foreach($query1->result_array('driver_details') as $row1){
												?><option value="<?php  echo $row1['id'];?>" ><?php  echo $row1['name']; ?></option>
												<?php
											 }
											
											?>
                                            </select>
                                        </div>
					
				</div>  
				  </form>
	          </div>
			  </div>
			  
                </div>
				</div>
				  
				   
			
      </div><!-- /.content-wrapper -->
     <?php
	 include"includes/admin-footer.php";
	 ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
   <script src="<?php echo base_url();?>assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
   
    
    <script src="<?php echo base_url();?>assets/adminlte/dist/js/demo.js"></script>
	<script src="<?php echo base_url();?>assets/adminlte/dist/js/app.js"></script>
	
	 <script src="<?php echo base_url();?>assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     <script src="<?php echo base_url();?>assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- page script -->
   
	  <script src="<?php echo base_url();?>assets/adminlte/dist/js/sb-admin-2.js"></script>
     <!-- page script -->
  
	 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
<script type="text/javascript">
	function Converttimeformat(time) 
		{
			var hrs = Number(time.match(/^(\d+)/)[1]);
			var mnts = Number(time.match(/:(\d+)/)[1]);
			var format = time.substr(time.length - 2);;
			if (format == "pm" && hrs < 12) hrs = hrs + 12;
			if (format == "am" && hrs == 12) hrs = hrs - 12;
			var hours = hrs.toString();
			var minutes = mnts.toString();
			if (hrs < 10) hours = "0" + hours;
			if (mnts < 10) minutes = "0" + minutes;		
			return hours;
			}
$(document).ready(function () {
		$(".sample").on("keydown", function (e) {
        return e.which !== 32;
	    });  
	
        $('.sample').keyup(function()
	     {
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
			var isSplChar = re.test(yourInput);
			if(isSplChar)
			{
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				$(this).val(no_spl_char);
			}
		});	
	 $( "#datepickerp" ).attr("placeholder", "mm-dd-yyyy").datepicker({
		    minDate: 0//this option for allowing user to select from year range
	   });
	   $("#datepickerp").change(function(){
		 	var date = $(this).val();
			$.ajax({
			url:'<?php echo base_url();?>callmycab/timepickera',
			data:{'date' : date},
			type:'post',
			success:function(result){
			$(".time_div").html(result);
			}	
			});
	   });
    
       var a = $('#transfer').val();
	    $("#trans").val(a);
	    if(a =='going') {
	    $("#drop").show();
	    $("#pick").hide();
	    } else {
	    $("#pick").show();
	    $("#drop").hide();
	    }
        $('#transfer').on('change', function (){
	    var a = $(this).val();
	    $("#trans").val(a);
	    if(a =='going') {
	    $("#drop").show();
	    $("#pick").hide();
	    } else {
	    $("#pick").show();
	    $("#drop").hide();
	    }
       });	
	    
 
        $('.regcom').on('change', function (){
	     var a = $(this).val();
	     if(a != '') {
	       $(this).removeClass('error-admin');
	      } else {
	      $(this).addClass('error-admin');
	      }
       });
 
 
	
        $("#useradd").click(function(){
			
         var username = $('#signup-username').val();
	      parea = $('#transfer1' ).val();
          darea = $('#autocomp' ).val();
          pdate = $('#datepickerp' ).val();
	      ptime = $('.times' ).val().match(/^([012]?\d):([0-6]?\d)\s*(a|p)m$/i);
	      paddress = $('#pickupadd' ).val();
		  car = $('#car').val();
	        driver = $('#assigned').val();
       if(!username){
	
	     $( "#signup-username" ).addClass('error-admin');
	     $("#signup-username").focus();
		 return false;
        }
		if(username=="0" ){

				alert("Please Select user");
				 return false;
			}
        if(!parea){
	    
	      $('#transfer1' ).addClass('error-admin');
	      $('#transfer1' ).focus();
		  return false;
        } 
		
		
        if(!pdate){
	   
	      $( "#datepickerp" ).addClass('error-admin');
	      $("#datepickerp").focus();
		  return false;
        }
        if(!ptime){
	   
	       $( ".times" ).addClass('error-admin');
	       $(".times").focus();
		   return false;
        }
         if(car=="0" ){

				alert("Please Select car");
				 return false;
			}
        if(!paddress){
            $('#pickupadd').addClass('error-admin');
            $("#pickupadd").focus(); 
            return false;
        }if(driver=="0" ){

				alert("Please Select driver");
				 return false;
			}
 
 
 	    var trans = $('#transfer').val();
		
        if(trans == 'going'){

         var pickup_time =$(".times").val() ;	
         var pickup_area =$("#transfer1").val() ;
         var drop_area =$("#autocomp").val() ;
        }else{

         var pickup_time =$(".times").val() ;
         var drop_area =$("#transfer1").val();
         var pickup_area =$("#autocomp").val() ;
        }
         var username = $('#signup-username').val();
         var pickup_date =$("#datepickerp").val() ;
         var purpose ="Airport Transfer";
         var taxi_type =$("#taxi_type").val() ;
         var area =$("#area1").val() ;
         var flight_number =$("#flight_number").val() ;
         var pickup_address = $("#pickupadd").val();
         $.ajax({
          url:'<?php echo base_url();?>admin/admin_book',
         method:'post',
          data:{'username':username,'taxi_type':taxi_type,'purpose':purpose,'pickup_area':pickup_area,'pickup_date':pickup_date,'drop_area':drop_area,'pickup_time':pickup_time,'area':area,'flight_number':flight_number,'taxi_type':taxi_type,'pickup_address':pickup_address},
          success:function(res){
	      $(".adminuser1").show();
          console.log(res);
          if(res==1){
			   $(".adminuser1").focus();
		       $(".adminuser1").html('<p class="error">Erro</p>');
	           setTimeout(function(){$(".adminuser1").hide(); }, 3000);
            }
			else{

				
				
				
				 $(".adminuser1").focus();
				$(".adminuser1").html('<p class="success">Atualizado com sucesso</p>');
				setTimeout(function(){$(".adminuser1").hide(); }, 1500);
				$('#user_reg')[0].reset();
			}
        }
           });
        });
   });


</script>
  </body>
</html>
