<?php 
ob_start();
if($username = $this->session->userdata('username') || $this->input->cookie('username', false)){
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
        <?php
	   $query = $this->db->query(" SELECT * FROM `settings` order by id DESC ");
		$row = $query->row('settings');
		
		$textFile= $row->languages;
     $extension = ".php";
    $filename='includes/'.$textFile.$extension;
 
     if (file_exists($filename)) {
     include 'includes/'.$textFile.$extension;
     }else{
    include 'includes/en_lang.php'; 
     }
	  ?>
      <title><?php echo $row->title; ?></title>
      <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
       <link href='<?php echo base_url();?>assets/css?family=Open+Sans' rel='stylesheet' type='text/css'>
      <!-- Bootstrap core CSS -->
      <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
      <!-- Custom styles for this template -->
      <link href="<?php echo base_url();?>assets/css/jumbotron.css" rel="stylesheet">
       <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
      <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
      <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
      <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script> <!-- Gem jQuery -->
         
      
      <script src="<?php echo base_url();?>assets/js/ie-emulation-modes-warning.js"></script>
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      
        <link rel="shortcut icon" type="image/icon" href="<?php echo base_url();?><?php echo $row->favicon; ?>"/>
      
      
    <!-- Login -->
    <link href='<?php echo base_url();?>assets/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'> 
 	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/login/style.css"> <!-- Gem style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar/component.css"> <!-- Gem style -->
	<script src="<?php echo base_url();?>assets/js/login/modernizr.js"></script> <!-- Modernizr -->
    <script src="<?php echo base_url();?>assets/js/login/main.js"></script> <!-- Gem jQuery -->
    
   
   
   
   
    
   
   
   
          <!--/column1-->
      <!-- /container -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
     
      
       


        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
   
   
   
   
    <script>
	  jQuery(document).ready(function(){
	  var analatic= jQuery('#analatic_code').val();
	  
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');


ga('create', analatic, 'auto');
ga('send', 'pageview');
});
</script>
   
   
   
      
      
   </head>
   <body>
   <input type='hidden' value='<?php echo $row->analatic_code;?>' id='analatic_code'>
                  
   <?php if($row->sidebar=='Vertical'){ ?>
   <ul class="cbp-vimenu">
              <li><a href="<?php if($type = $this->session->userdata('type')=="user"){ echo base_url();}?>" class="icon-logo"><?php echo $logos; ?></a></li>
                     
            
              <li class="dfdfd"><a href="javascript:void(0);" class="icon-freechart parafair" data-toggle="modal" data-target="#myModal"><?php echo $fare_charts; ?></a></li>
              <li><a href="<?php echo base_url();?>callmycab/page_index/about_us" class="icon-about"> <?php echo $abouts; ?> </a></li>
                                
                       <?php  if($username = $this->session->userdata('username')){
		$value = $this->session->userdata('username');
		}else{
		$value= $this->input->cookie('username', false);
		}
						if(!empty($value)){
							?>
                              <li ><a href="<?php echo base_url();?>callmycab/logout" class="icon-logout "><?php echo $log_out; ?></a></li>
                        <?php
						}else{?>
              <nav class="main-nav">
              <li ><a href="javascript:void(0);" class="icon-login sess-login1"><?php echo $Logins_Registers; ?></a></li>
              </nav>
             <?php
						}
						?>
              </ul>
   <?php
   }?>
     <div class="jumbotron <?php if($row->sidebar=='Vertical'){ echo "verticalcal";}?>">
      
     
         <div class="bnr">
		 <?php
	 if($row->sidebar=='Horizontal'){
		 ?>
		 
		<div class="headermenus">
	<nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a href="<?php if($type = $this->session->userdata('type')=="user"){ echo base_url();}?>" class="navbar-brand"><img class='circle'src="<?php echo base_url();?>upload/logo.png"></a>
         </div>
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
		  <?php  if($username = $this->session->userdata('username')){
		$value = $this->session->userdata('username');
		}else{
		$value= $this->input->cookie('username', false);
		}
		if(!empty($value)){
		?>
		
            
			<li><a href="<?php echo base_url();?>callmycab/logout" >
				<span class="menu-image6"><?php echo $log_out; ?>  </span>
			</a></li>
			<?php
		}else{
			?>
			<li><nav class="main-nav"><a href="javascript:void(0);" class='sess-login11'>
				<span class="menu-image1"><img src="<?php echo base_url();?>assets/images/menu-icon1.png"></span>
				<span class='sess-login1'><?php echo $Logins_Registers; ?></span>
			</a></nav></li>
			<?php
		}
		?>
           
			
            <li><a href="javascript:void(0);" class='parafair' data-toggle="modal" data-target="#myModal">
				<span class="menu-image3"><img src="<?php echo base_url();?>assets/images/menu-icon5.png"></span>
				<span><?php echo $fare_charts; ?></span>
			</a></li>
			<li><a href="<?php echo base_url();?>callmycab/page_index/about_us">
				<span class="menu-image3"><img src="<?php echo base_url();?>assets/images/menu-icon3.png"></span>
				<span><?php echo $abouts; ?></span>
			</a></li>
            <li><a class="bordernone" href="<?php echo base_url();?>callmycab/page_index/contact_us">
				<span class="menu-image5"><img src="<?php echo base_url();?>assets/images/menu-icon4.png"></span>
				<span><?php echo $contacts; ?></span>
			</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>	
	
          <?php
	 }
	 include "includes/myaccount_header.php";
	 include "includes/farechart.php"; 
	 ?>
     
            <!--/.navbar-collapse -->
         </div>
      </nav>
      <!-- Main jumbotron for a primary marketing message or call to action -->
    
            <div class="container">
            <div class="secssion1">

               
                <!-- My Account -->
               <div class="col-md-12">
               		<div class="row">
               		<div class="myaccount">
                    
                    <div class="col-md-4"> 
                   	<div class="profile_outr">
                    	<div class="myacunt_hd">
                        
                        	<div class="prfl_hd1">
                            	<div class="profl_img"> <img src="<?php echo base_url();?>assets/images/profile_pic.jpg" alt=""> </div> <h1> <?php echo $profiles; ?> </h1> <p><?php echo $personal_detaile; ?></p>  
                            
                            
                            </div>
                            <nav class="main-nav1">
                            <div class="prfl_hd2"><?php echo $change_password; ?></div> 
                            </nav>
                        
                        </div>
                        
                        <div class="myacunt_area">
                        
         <?php
		 if($this->session->userdata('username')){
		$username=$this->session->userdata('username');
		}else{
			$username= $this->input->cookie('username', false);
		}
		 $type =$this->session->userdata('type');
		 if($type=="user"){
			 $query = $this->db->query("SELECT * FROM  userdetails WHERE username='$username'"); 
			 $row = $query->row('userdetails');
			 $mobile =$row->mobile;
		 }else{
			  $query = $this->db->query("SELECT * FROM  driver_details WHERE user_name='$username'");
			  $row = $query->row('driver_details');
			   $mobile =$row->phone;
		 }
		
		
		 ?>

                        <form method="post" class="contact-form">
                        
                        <h1><?php echo $contact_information; ?></h1>
                        <p class="field_hd"> <?php echo $names; ?> </p>
                        <input type="text" placeholder="Starting " name="username" class="field_myacnt readonly" value="<?php echo $username; ?>" readonly>
                        
                        <p class="field_hd"><?php echo $mobiles_numbers; ?></p>
                        <input type="text" placeholder="Starting" name="mobile" class="field_myacnt contact-mobile readonly" value="<?php echo $mobile;  ?>" readonly>
                        
                        <p class="field_hd"> <?php echo $emails; ?> </p>
                        <input type="text" placeholder="Starting" name="email" class="field_myacnt contact-email readonly" value="<?php echo $row->email; ?>"readonly>
                        
                        
                        <h1><?php echo $basics_informations; ?></h1>
                        <p class="field_hd"> <?php echo $genders; ?> </p>
                        <div class="field_radio">
                        <input type="radio" name="gender" class="field_radio contact-gender" value="male"  <?php if($row->gender=='male') {echo "checked";} ?>/> <?php echo $males; ?>
						<input type="radio" name="gender" class="field_radio contact-gender" value="female"  <?php if($row->gender=='female'){echo "checked";} ?>/> <?php echo $females; ?>  
 <p class="text-gender"></p><p class="field_hd"><b>Saldo atual :$</b><?php if($row->wallet_amount!=0){echo $row->wallet_amount;}else{echo "0";} ?></p>
                        </div>
                        
                        
                        <p class="field_hd"><?php echo $dob; ?></p>
                        <input type="text" placeholder="DD/MM/YYYY" name="dob" class="field_myacnt datepicker regcom" readonly value="<?php echo $row->dob; ?>" >
                        
                        <p class="field_hd"><?php echo $Anniversary; ?></p>
                        <input type="text" placeholder="DD/MM/YYYY" name="anniversary_date" readonly class="field_myacnt datepicker regcom" value="<?php echo $row->anniversary_date; ?>">
                        
                        <input type="button" class="updatebtn updateuser" value="Atualizar">
                      
                       <a href="#"> <div class="cancelbtn"> <?php echo $cancel; ?> </div></a>
                      
                        </form>
                       
                        </div>
                        
                       
                    </div>
                    </div>
                    
                    
                    
                    <div class="col-md-8"> 
                    <div class="booking_outr">
                    
                    <div class="booking_hd"> 
                   <!--  /* <div class="bkng_cl1"> <h1> BOOKING DETAILS </h1>
                        <ul class="tab-links">
                         <li class="book-status active">
                           <a href="#" title='Booking'>Active</a>
                         </li>
                         <li class="book-status">
                            <a href="#" title='Cancelled'>Past</a>
                         </li>
                         </ul>*/-->
                         
                         
                       <!--  </div>
                        <div class="bkng_cl2"> <input type="text" placeholder="" name="searchdate" class="field-srch datepicker1"> </div>
                        <div class="bkng_cl3"></div>-->
                    
                   <div class="bkng_cl1"> <h1><?php echo $bookings_detailes; ?></h1> </div>
                     <div class="bkng_cl2">   <div class="my_actv book-status active1"> <a href="#" title='Booking' item="Processing"><?php echo $actives; ?></a> </div> 
					 <div class="my_past book-status "> <a href="#" title='Cancelled' item="Completed"><?php echo $pasts; ?></a> </div></div>
                     <div class="bkng_cl3"><input type="text"  name="searchdate" placeholder="" class=" field-srch  datepicker1">
                     </div>
                     
                    
                    </div>
                    <div class="table updateuser1" id="table1">
                    <div class="data">
                    
                    <!-- Box 1 -->
                    


</div>
<div class="pagination1"></div>


</div>


































         
         
          
           
		




























































                    
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    </div>
                    
                    
                    
                    </div> 
                    </div>
                    
                    
                    </div>
                    </div>
               </div>
               <!--/My Account--> 
               
               
                 
                  
               </div>
            </div>
         </div>
 
      <!-- column1-->
      <!--/column1-->
      
      
      <!-- column2-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Minified JavaScript -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script> 
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

   <script type="text/javascript">
            $(document).ready(function(){
                  

                function loadData(page){
                      var status = $('.book-status.active1').find("a").attr('title');
					var item = $('.book-status.active1').find("a").attr('item');
					if($('.datepicker1').val()!=0){
					 date = $('.datepicker1').val();  
					}else{
						 date = 0;  
					}
					 'page='+page ; 
					
                    $.ajax
                    ({
                        type: "POST",
                        url: "<?php echo base_url();?>callmycab/search_s",
                        data: {'page':page,'status':status,'item':item,'date':date},
						success:function(msg){
							  //alert(result);
							 // alert(status);
							   //alert(date);
							    $("#table1").ajaxComplete(function(event, request, settings)
                            {
							    
							   $("#table1").html(msg);
							     });	
						}
							  
							   });	
                        	  
                   $('.datepicker1').live('change',function(){
										var status = $('.book-status.active1').find("a").attr('title');
										var item = $('.book-status.active1').find("a").attr('item');
										if($('.datepicker1').val()!=0){
					 date = $('.datepicker1').val();  
					}else{
						 date = 0;  
					}
										'page='+page ; 
										 loadData(page,status,item,date);
});		
$('.book-status').live('click',function(){
					var status = $(this).find("a").attr('title');
					var item = $(this).find("a").attr('item');
					
					$('.book-status').removeClass('active1');
					if($('.datepicker1').val()!=0){
					 date = $('.datepicker1').val();  
					}else{
						 date = 0;  
					}
					$(this).addClass('active1');
					 'page='+page ; 
					 loadData(page,status,item,date);		  
});			     
                    
                }
                loadData(1,'Booking','Processing',0);  // For first time page load default results
                $('.pagination1 li.active').live('click',function(){
					 var status = $('.book-status.active1').find("a").attr('title');
					 var item = $('.book-status.active1').find("a").attr('item');
					var date;
					if($('.datepicker1').val()!=0){
					 date = $('.datepicker1').val();  
					}
                    var page = $(this).attr('p');
                    loadData(page,status,item,date);
                    
                });           
                $('#go_btn').live('click',function(){
                    var page = parseInt($('.goto').val());
                    var no_of_pages = parseInt($('.total').attr('a'));
                    if(page != 0 && page <= no_of_pages){
                        loadData(page);
                    }else{
                        alert('Enter a PAGE between 1 and '+no_of_pages);
                        $('.goto').val("").focus();
                        return false;
                    }
                    
                });
            });
        </script>    
      
  <script type="text/javascript">
  
 
$(document).ready(function(){ 

			   
						   
						   
	
 
$('.updateuser').click(function(){	
	

var mobile = $('.contact-mobile').val();
var gender = $("input[type='radio'].contact-gender:checked").val();

 var pattern = /^\d{10}$/;
 if(!pattern.test(mobile)){
	   $(".contact-mobile").addClass('required');
	    $(".contact-mobile").focus();
		return false;
   }
if(!gender){
	alert('Please select gender');
	$(".contact-gender").focus();
		return false;
}

								
								
								
var value =$(".contact-form").serialize() ;

$.ajax({
url:'<?php echo base_url();?>callmycab/contact',
type:'post',
data:value,
success:function(contact){
console.log(contact);
if(contact==1){
alert('profile details updated successfully');
}
else{
alert('profile update fail');
}
}
});  
});  
											 
											 
											 
											 
						
	
						
						
						
						
						
											 
											 
											 });
			
	
			
			</script>
    
    
      
      <!--/column1-->
      <!-- /container -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
     <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-timepicker.css"/>
<!-- Load jQuery JS -->
<script src="<?php echo base_url();?>assets/js/jquery-timepicker.js"></script>
<!-- Load jQuery UI Main JS -->
<script src="<?php echo base_url();?>assets/js/jquery-timepicker-min.js"></script>
<!--end timepicker-->

      <!-- Placed at the end of the document so the callmycab load faster -->
      <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="<?php echo base_url();?>assets/js/ie10-viewport-bug-workaround.js"></script>
      
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
        <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
        
       <script type="text/javascript">
	  $(document).ready(function () {
							 
							 $(".datepicker1").datepicker({
														  showOn: 'button',
	     buttonText: 'Show Date',
	     buttonImageOnly: true,
	     buttonImage: '<?php echo base_url();?>assets/images/myaccount_calnder.png'
	  });	  
				
				
		$( ".datepicker" ).datepicker({
																dateFormat : 'dd-mm-yy',
		changeMonth: true,
        changeYear: true,
		yearRange: '1910:2010'
																//this option for allowing user to select from year range
																});	
				
				
				
	   


});
	  </script>
      
      
   </body>
</html>
<?php
}else{
	 redirect('/', 'refresh');
}

?>