<!DOCTYPE html>
<html>
        <?php
	           include"includes/admin_header.php";
	    ?>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <?php
	           include"includes/admin_sidebar.php";
	    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class="add_promocode">
             Adicionar idioma
          </h1>
		  
		 
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">language</a></li>
            <li class="active">Adicionar</li>
          </ol>
        </section>

        <!-- Main content -->
		
		<div class="">
			    <div class="">
                    <div class="col-lg-6">
                <div class="box box-primary edit_promoform1">
				 
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
				   <div class="edituser" tabindex='1'></div>
                </div><!-- /.box-header -->
                <!-- form start -->
				
                <form role="form"  method="post" id="taxi_reg">
				        <div class="box-body">
				                        <div class="form-group">
                                           <label>Add Language</label>
                                           <input class="form-control regcom sample" placeholder="Add Language" name="languages"  type="text">
                                        </div>
										<div class="form-group">
                                           <label>Point To Point </label>
                                           <input class="form-control regcom" placeholder="Point To Point" name="languagesw" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Airport Transfer</label>
                                           <input class="form-control regcom" placeholder="Airport Transfer" name="Airport" type="text" >
                                        </div>
										<div class="form-group">
                                           <label>Hourly Rental</label>
                                           <input class="form-control regcom" placeholder="Hourly Rental" name="Hourly" type="text"  >
                                        </div>
                                         
										<div class="form-group">
                                           <label>Outstation</label>
                                           <input class="form-control regcom" placeholder="Outstation" name="Outstations" type="text" >
                                        </div>
										
										<div class="form-group">
                                           <label>Drop Area</label>
                                           <input class="form-control regcom" placeholder="Drop Area" name="DropArea" type="text"  >
                                        </div>
										<div class="form-group">
                                            <label>Pickup Time</label>
                                           <input class="form-control regcom" placeholder="Pickup Time" name="PickupTime" type="text" >
                                        </div>
										
										 <div class="form-group">
                                           <label>Select Car</label>
                                           <input class="form-control regcom" placeholder="Select Car" name="SelectCar" type="text" >
                                        </div>
										
									    <div class="form-group">
                                           <label>Fare</label>
                                           <input class="form-control regcom" placeholder="fare" name="fare" type="text"  >
                                        </div>
										
										<div class="form-group">
                                           <label>Fare Detail</label>
                                           <input class="form-control regcom" placeholder="Fare Detail" name="FareDetail" type="text"  >
                                        </div>
										
								
										
										<div class="form-group">
                                           <label>Enter a promo code (Optional)</label>
                                           <input class="form-control regcom" placeholder="Enter a promo code" name="Enter_apromocode" type="text"  >
                                        </div>
										<div class="form-group">
                                           <label>One Way Trip</label>
                                           <input class="form-control regcom" placeholder="One Way Trip" name="one_waytrip" type="text"  >
                                        </div>
										
										<div class="form-group">
                                           <label>Round Trip</label>
                                           <input class="form-control regcom" placeholder="Round Trip" name="round_trip" type="text"  >
                                        </div>
										
									    <div class="form-group">
                                           <label>To</label>
                                           <input class="form-control regcom" placeholder="To" name="to" type="text" >
                                        </div>
										
										<div class="form-group">
                                           <label>Departure Date</label>
                                           <input class="form-control regcom" placeholder="Departure Date" name="departure_date" type="text"  >
                                        </div>
										
								
                                        <div class="form-group">
                                           <label>Callback</label>
                                           <input class="form-control regcom" placeholder="Callback " name="callback" type="text">
                                        </div>
										  <div class="form-group">
                                            <label>Farechart</label>
                                           <input class="form-control regcom" placeholder="Farechart" name="farechart" type="text">
                                        </div>
										<div class="form-group">
                                           <label>About</label>
                                           <input class="form-control regcom" placeholder="About" name="about" type="text" >
                                        </div>
                                        <div class="form-group">
                                           <label>Contact</label>
                                           <input class="form-control regcom" placeholder="Contact" name="contact" type="text" >
                                        </div>
										<div class="form-group">
                                           <label>Please Login/Register to apply the Promo Code</label>
                                           <input class="form-control regcom" placeholder="Please Login/Register to apply the Promo Code" name="please_login" type="text"  >
                                        </div>	



                     
						

                                        <div class="form-group">
                                           <label>Change Password</label>
                                           <input class="form-control regcom" placeholder="Change Password " name="change_password" type="text" >
                                        </div>
									    <div class="form-group">
                                           <label>Contact Information</label>
                                           <input class="form-control regcom" placeholder="Contact Information" name="contact_information" type="text" >
                                        </div>
										<div class="form-group">
                                           <label>Name</label>
                                           <input class="form-control regcom" placeholder="Name" name="name" type="text"  >
                                        </div>
                                        <div class="form-group">
                                           <label>Mobile Number</label>
                                           <input class="form-control regcom" placeholder="Mobile Number" name="mobile_number" type="text"  >
                                        </div>
										<div class="form-group">
                                           <label>Email</label>
                                           <input class="form-control regcom" placeholder="Email" name="email" type="text"  >
                                        </div>	

										<div class="form-group">
                                           <label>Anniversary Date</label>
                                           <input class="form-control regcom" placeholder="Anniversary Date" name="anivdate" type="text" >
                                        </div>
				                        <div class="form-group">
                                           <label>Cancel</label>
                                           <input class="form-control regcom" placeholder="Cancel" name="cancel" type="text" >
                                        </div>
										  <div class="form-group">
                                            <label>BOOKING DETAILS</label>
                                           <input class="form-control regcom" placeholder="BOOKING DETAILS" name="bkdetail" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Active</label>
                                           <input class="form-control regcom" placeholder="Active" name="active" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Past</label>
                                           <input class="form-control regcom" placeholder="Past" name="Past" type="text" >
                                        </div>

										
										<div class="form-group">
                                            <label>Close</label>
                                           <input class="form-control regcom" placeholder="Close" name="Close" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Sign in</label>
                                           <input class="form-control regcom" placeholder="Sign in" name="signin" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>New account</label>
                                           <input class="form-control regcom" placeholder="New account" name="newact" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Username</label>
                                           <input class="form-control regcom" placeholder="Username" name="Username" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Location</label>
                                           <input class="form-control regcom" placeholder="location" name="locations" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Select Taxi</label>
                                           <input class="form-control regcom" placeholder="Select Taxi" name="SelectTaxi" type="text" >
                                        </div>
                                       <div class="form-group">
                                       
                                        <input type="button" class="btn btn-primary" value="Submit"  name="Save" id="useradd">
                                        <button type="reset" class="btn btn-primary">Reset </button>
                                        </div>             
                                     
				        </div>
	                </div>
                </div><!-- /.box -->
	
			 <div class="col-lg-6">
			   <div class="box box-primary edit_promoform">
				 
                <div class="box-header with-border">
                   <h3 class="box-title"></h3>
                      </div><!-- /.box-header -->
                <!-- form start -->
				
                
				
                <div class="box-body">
				  
					                   
										 
										 <div class="form-group">
                                            <label>Confirm Booking</label>
                                           <input class="form-control regcom" placeholder="Confirm Booking" name="ConfirmBooking" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Pickup Area</label>
                                           <input class="form-control regcom" placeholder="Pickup Area" name="PickupArea" type="text">
                                        </div>
										 <div class="form-group">
                                            <label>Pickup Date</label>
                                           <input class="form-control regcom" placeholder="Pickup Date" name="PickupDate" type="text"  >
                                        </div>

                                         <div class="form-group">
                                            <label>Area</label>
                                           <input class="form-control regcom" placeholder="Area" name="area" type="text">
                                        </div>
										 <div class="form-group">
                                            <label>Landmark</label>
                                           <input class="form-control regcom" placeholder="Landmark" name="landmark" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Pickup Address</label>
                                           <input class="form-control regcom" placeholder="Pickup Address" name="pickupaddress" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Flight Number</label>
                                           <input class="form-control regcom" placeholder="Flight Number" name="flightnumber" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Package</label>
                                           <input class="form-control regcom" placeholder="Package" name="package" type="text" >
                                        </div>
									
										<div class="form-group">
                                            <label>Return Date</label>
                                           <input class="form-control regcom" placeholder="Return Date" name="returndate" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Cab Details Not Available</label>
                                           <input class="form-control regcom" placeholder="Cab details not available" name="cabdetails_notavailable" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Going to Airport</label>
                                           <input class="form-control regcom" placeholder="Going to Airport" name="going_toairport" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Coming from Airport</label>
                                           <input class="form-control regcom" placeholder="Coming from Airport" name="coming_fromairport" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Login/Register</label>
                                           <input class="form-control regcom" placeholder="Login/Register" name="login_register" type="text"  >
                                        </div>
										
										
										
										<div class="form-group">
                                            <label>Logo</label>
                                           <input class="form-control regcom" placeholder="Logo" name="logo" type="text" >
                                        </div>
										
										 <div class="form-group">
                                            <label>Logout</label>
                                           <input class="form-control regcom" placeholder="Logout" name="logout" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Profile</label>
                                           <input class="form-control regcom" placeholder="PROFILE" name="profile" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Personal Details</label>
                                           <input class="form-control regcom" placeholder="Personal Details" name="personal_details" type="text"  >
                                        </div>
										
										
										<div class="form-group">
                                            <label>Basic Information</label>
                                           <input class="form-control regcom" placeholder="Basic Information" name="basic_information" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Gender</label>
                                           <input class="form-control regcom" placeholder="Gender" name="gender" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Male</label>
                                           <input class="form-control regcom" placeholder="Male" name="male" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Female</label>
                                           <input class="form-control regcom" placeholder="Female" name="female" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Date of Birth</label>
                                           <input class="form-control regcom" placeholder="Date of Birth" name="dob" type="text" >
                                        </div>
										
				                
										<div class="form-group">
                                            <label>Enter Verification Code</label>
                                           <input class="form-control regcom" placeholder="Enter Verification Code" name="EVC" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Error Message Here!</label>
                                           <input class="form-control regcom" placeholder="Error message here!" name="EMH" type="text" >
                                        </div>
			                           <div class="form-group">
                                            <label>Didn't Receive OTP? </label>
                                           <input class="form-control regcom" placeholder="Didn't receive OTP? " name="DRO" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Re-Send </label>
                                           <input class="form-control regcom" placeholder="Re-Send" name="resend" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Please check your email for verification code</label>
                                           <input class="form-control regcom" placeholder="Please check your email for verification code" name="PCEV" type="text" >
                                        </div>
										
										<div class="form-group">
                                            <label>Password</label>
                                           <input class="form-control regcom" placeholder="Password" name="Password" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Hide</label>
                                           <input class="form-control regcom" placeholder="Hide" name="Hide" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Remember me</label>
                                           <input class="form-control regcom" placeholder="Remember me" name="remenber" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Forgot your password?</label>
                                           <input class="form-control regcom" placeholder="Forgot your password?" name="FYP" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Save This Address</label>
                                           <input class="form-control regcom" placeholder="Save This Address" name="save_addr" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Pay Now</label>
                                           <input class="form-control regcom" placeholder="Pay Now" name="PayNow" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Book Now</label>
                                           <input class="form-control regcom" placeholder="Book Now" name="BookNow" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Find my Taxi</label>
                                           <input class="form-control regcom" placeholder="Find my Taxi" name="FindmyTaxi" type="text" >
                                        </div>
										</form> 
	      </div>
	  </div>
	  
	  
	  
	  
	  
	  
	  
	  
			  
                    <!-- /.panel -->
                </div>
				</div>
             
					                   
				
				</div>
				
				</div>
      <!-- /.content-wrapper -->
     <?php
	 include"includes/admin-footer.php";
	 ?>

     
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/adminlte/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>assets/adminlte/dist/js/demo.js"></script>
    <!-- page script -->
    
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    
    <script type="text/javascript">
$(document).ready(function(){
	   
						   
$('.regcom').on('change', function (){
var a = $(this).val();
if(a != '') {
$(this).removeClass('error-admin');

} else {
$(this).addClass('error-admin');
return false;
}
 });						   
$(".sample").on("keydown", function (e) {
        return e.which !== 32;
	    });  
	
        $('.regcom').keyup(function()
	     {
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
			var isSplChar = re.test(yourInput);
			if(isSplChar)
			{
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				$(this).val(no_spl_char);
			}
		});							   
						   
 $("#useradd").click(function(e){

  var isValid = true;
        $('input[type="text"]').each(function() {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    
                });
				$(this).focus();
				return false;
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
		 if (isValid == false) {
            e.preventDefault();
		 }
        else {
            
	          var value =$("#taxi_reg").serialize() ;

$.ajax({
url:'<?php echo base_url();?>admin/insert_language',
method:'post',
data:value,
success:function(res){
$(".edituser").show();
console.log(res);
if(res==0){
	 $(".edituser").focus();
	$(".edituser").html('<p class="error">Erro</p>');
	setTimeout(function(){$(".edituser").hide(); }, 3000);
	
}else if(res==2){
	
	$(".edituser").focus();
	$(".edituser").html('<p class="error">Idioma existente</p>');
	setTimeout(function(){$(".edituser").hide(); }, 3000);
}
else{
$(".edituser").focus();
$(".edituser").html('<p class="success">Salvo com sucesso</p>');
setTimeout(function(){$(".edituser").hide(); }, 1500);
$('#taxi_reg')[0].reset();
}
}
}); 
		}
	            
                                       
 
});
});

</script>


  </body>
</html>
