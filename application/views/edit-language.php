<!DOCTYPE html>
<html>
  <?php
	 include"includes/admin_header.php";
	 ?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Left side column. contains the logo and sidebar -->
     <?php
	 include"includes/admin_sidebar.php";
	 ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
                        
               <h1 class="edit_promo">
                  Edit Language Details
               </h1>
                        
                      
		  
		 
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">language</a></li>
            <li class="active">Edit</li>
          </ol>
        </section>

        <!-- Main content -->
 <div class="">
			   <div class="">
                <div class="col-lg-6">
           <div class="box box-primary edit_promoform1">
				 
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
				   <div class="edituser" tabindex='1'></div>
                </div><!-- /.box-header -->
                <!-- form start -->
				  <?php
								 $id = $_GET['idp'];
						  
		 $query = $this->db->query("SELECT * FROM  language_set WHERE  id ='$id'");
		 $row = $query->row('language_set');
		 $textFile = $row->languages;
         $extension = ".php";     
  
		 include 'includes/'.$textFile.$extension;
								?>
               <form role="form"  method="post" id="taxi_reg">
				
                  <div class="box-body">
				              <div class="form-group">
                                            <label>Language Name</label>
                                           <input class="form-control regcom sample" placeholder="Language Name" name="languages" id="signup-username" value="<?php echo $row->languages; ?>" >
                                        </div>
                                        <div class="form-group">
                                            <label>Point To Point </label>
                                           <input class="form-control regcom" value="<?php echo $point_topoint;?>" name="languagesw" type="text" id="languagesw" value="<?php echo $point_topoint;?>">
                                        
										</div>
							            <div class="form-group">
                                            <label>Airport Transfer</label>
                                           <input class="form-control regcom" value="<?php echo $airport_transfer;?>" name="Airport" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Hourly Rental</label>
                                           <input class="form-control regcom" value="<?php echo $hourly_rental;?>" name="Hourly" type="text"  >
                                        </div>
                                         
										  <div class="form-group">
                                            <label>Outstation</label>
                                           <input class="form-control regcom" value="<?php echo $out_station;?>" name="Outstations" type="text" >
                                        </div>
										
										<div class="form-group">
                                            <label>Drop Area</label>
                                           <input class="form-control regcom" value="<?php echo $drop_area;?>" name="DropArea" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Pickup Time</label>
                                           <input class="form-control regcom" value="<?php echo $pickup_time;?>" name="PickupTime" type="text" >
                                        </div>
										
										 <div class="form-group">
                                            <label>Select Car</label>
                                           <input class="form-control regcom" value="<?php echo $select_car;?>" name="SelectCar" type="text" >
                                        </div>
										
									    <div class="form-group">
                                           <label>Fare</label>
                                           <input class="form-control regcom" value="<?php echo $Fares;?>" name="fare" type="text"  >
                                        </div>
										
										<div class="form-group">
                                           <label>Fare Detail</label>
                                           <input class="form-control regcom"  value="<?php echo $fare_detail;?>" name="FareDetail" type="text"  >
                                        </div>
										
								
										
										<div class="form-group">
                                            <label>Enter a promo code (Optional)</label>
                                           <input class="form-control regcom"  value="<?php echo $promocodes;?>" name="Enter_apromocode" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>One Way Trip</label>
                                           <input class="form-control regcom"  value="<?php echo $oneway_trip;?>" name="one_waytrip" type="text"  >
                                        </div>
										
										 <div class="form-group">
                                            <label>Round Trip</label>
                                           <input class="form-control regcom" value="<?php echo $roundtrips;?>" name="round_trip" type="text"  >
                                        </div>
										
									    <div class="form-group">
                                           <label>To</label>
                                           <input class="form-control regcom" value="<?php echo $to;?>" name="to" type="text" >
                                        </div>
										
										<div class="form-group">
                                           <label>Departure Date</label>
                                           <input class="form-control regcom" value="<?php echo $departure_date;?>" name="departure_date" type="text"  >
                                        </div>
										
								
                                        <div class="form-group">
                                           <label>Callback</label>
                                           <input class="form-control regcom" value="<?php echo $callbacks;?>" name="callback" type="text">
                                        </div>
										  <div class="form-group">
                                            <label>Farechart</label>
                                           <input class="form-control regcom" value="<?php echo $fare_charts;?>" name="farechart" type="text">
                                        </div>
										 <div class="form-group">
                                            <label>About</label>
                                           <input class="form-control regcom" value="<?php echo $abouts;?>" name="about" type="text" >
                                        </div>
                                        <div class="form-group">
                                            <label>Contact</label>
                                           <input class="form-control regcom" value="<?php echo $contacts;?>" name="contact" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Please Login/Register to apply the Promo Code</label>
                                           <input class="form-control regcom" value="<?php echo $pls_login_registers;?>" name="please_login" type="text"  >
                                        </div>	



                     
						

                                        <div class="form-group">
                                           <label>Change Password</label>
                                           <input class="form-control regcom"  value="<?php echo $change_password;?>" name="change_password" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Contact Information</label>
                                           <input class="form-control regcom" value="<?php echo $contact_information;?>" name="contact_information" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Name</label>
                                           <input class="form-control regcom" value="<?php echo $names;?>" name="name" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                           <input class="form-control regcom" value="<?php echo $mobiles_numbers;?>" name="mobile_number" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Email</label>
                                           <input class="form-control regcom" value="<?php echo $emails;?>" name="email" type="text"  >
                                        </div>						
										
                                     

                  						<div class="form-group">
                                            <label>Anniversary Date</label>
                                           <input class="form-control regcom" value="<?php echo $Anniversary;?>" name="anivdate" type="text" >
                                        </div>
			                             <div class="form-group">
                                            <label>Cancel</label>
                                           <input class="form-control regcom" value="<?php echo $cancel;?>" name="cancel" type="text" >
                                        </div>
										  <div class="form-group">
                                            <label>BOOKING DETAILS</label>
                                           <input class="form-control regcom" value="<?php echo $bookings_detailes;?>" name="bkdetail" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Active</label>
                                           <input class="form-control regcom" value="<?php echo $actives;?>" name="active" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Past</label>
                                           <input class="form-control regcom"  value="<?php echo $pasts;?>" name="Past" type="text" >
                                        </div>	
										
										
										 <div class="form-group">
                                            <label>Close</label>
                                           <input class="form-control regcom" value="<?php echo $close;?>" name="Close" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Sign in</label>
                                           <input class="form-control regcom" value="<?php echo $sign_in;?>" name="signin" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>New account</label>
                                           <input class="form-control regcom" value="<?php echo $new_accounts;?>" name="newact" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Username</label>
                                           <input class="form-control regcom" value="<?php echo $username;?>" name="Username" type="text" >
                                        </div>
										
                                        <div class="form-group">
                                            <label>Location</label>
                                           <input class="form-control regcom" value="<?php echo $locations;?>" name="locations" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Select Taxi</label>
                                           <input class="form-control regcom" value="<?php echo $select_taxi;?>" name="SelectTaxi" type="text" >
                                        </div>
                                         <div class="form-group">
                                       
                                        <input type="button" class="btn btn-primary" value="Save Details"  name="Save" id="useredit">
                                        
                                      
                                        <input  name="id"   value="<?php echo $row->id; ?>" type="hidden">
                                       </div>    
                                      
							
				</div> 









				
				  
	            </div>
		     </div><!-- /.box -->
			 
           </div>
		   
		   
		   
		   
			 <div class="col-lg-6">
			   <div class="box box-primary edit_promoform">
				 
                <div class="box-header with-border">
                   <h3 class="box-title"></h3>
                      </div><!-- /.box-header -->
                <!-- form start -->
				
                
				
                <div class="box-body">
				  
					                   
										 
										 <div class="form-group">
                                            <label>Confirm Booking</label>
                                           <input class="form-control regcom" value="<?php echo $confirm_booking;?>" name="ConfirmBooking" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Pickup Area</label>
                                           <input class="form-control regcom" value="<?php echo $pickup_area;?>" name="PickupArea" type="text">
                                        </div>
										 <div class="form-group">
                                            <label>Pickup Date</label>
                                           <input class="form-control regcom" value="<?php echo $pickup_date;?>" name="PickupDate" type="text"  >
                                        </div>

                                         <div class="form-group">
                                            <label>Area</label>
                                           <input class="form-control regcom" value="<?php echo $area;?>" name="area" type="text">
                                        </div>
										 <div class="form-group">
                                            <label>Landmark</label>
                                           <input class="form-control regcom" value="<?php echo $landmarks;?>" name="landmark" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Pickup Address</label>
                                           <input class="form-control regcom" value="<?php echo $pickups_addr;?>" name="pickupaddress" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Flight Number</label>
                                           <input class="form-control regcom" value="<?php echo $flight_number;?>" name="flightnumber" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Package</label>
                                           <input class="form-control regcom" value="<?php echo $Packages;?>" name="package" type="text" >
                                        </div>
									
										<div class="form-group">
                                            <label>Return Date</label>
                                           <input class="form-control regcom" value="<?php echo $return_date;?>" name="returndate" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Cab Details Not Available</label>
                                           <input class="form-control regcom" value="<?php echo $cab_not_available;?>" name="cabdetails_notavailable" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Going to Airport</label>
                                           <input class="form-control regcom" value="<?php echo $going_airport;?>" name="going_toairport" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Coming from Airport</label>
                                           <input class="form-control regcom" value="<?php echo $Coming_from_Airports;?>" name="coming_fromairport" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Login/Register</label>
                                           <input class="form-control regcom" value="<?php echo $login_Register;?>" name="login_register" type="text"  >
                                        </div>
										
										
										
										<div class="form-group">
                                            <label>Logo</label>
                                           <input class="form-control regcom" value="<?php echo $logos;?>" name="logo" type="text" >
                                        </div>
										
										 <div class="form-group">
                                            <label>Logout</label>
                                           <input class="form-control regcom" value="<?php echo $log_out;?>" name="logout" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Profile</label>
                                           <input class="form-control regcom" value="<?php echo $profiles;?>" name="profile" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Personal Details</label>
                                           <input class="form-control regcom" value="<?php echo $personal_detaile;?>" name="personal_details" type="text"  >
                                        </div>
										
										
										<div class="form-group">
                                            <label>Basic Information</label>
                                           <input class="form-control regcom" value="<?php echo $basics_informations;?>" name="basic_information" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Gender</label>
                                           <input class="form-control regcom" value="<?php echo $genders;?>" name="gender" type="text" >
                                        </div>
										 <div class="form-group">
                                            <label>Male</label>
                                           <input class="form-control regcom" value="<?php echo $males;?>" name="male" type="text"  >
                                        </div>
                                        <div class="form-group">
                                            <label>Female</label>
                                           <input class="form-control regcom" value="<?php echo $females;?>" name="female" type="text"  >
                                        </div>
										 <div class="form-group">
                                            <label>Date of Birth</label>
                                           <input class="form-control regcom" value="<?php echo $dob;?>" name="dob" type="text" >
                                        </div>
										
										<div class="form-group">
                                            <label>Enter Verification Code</label>
                                           <input class="form-control regcom" value="<?php echo $verification_code;?>" name="EVC" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Error Message Here!</label>
                                           <input class="form-control regcom" value="<?php echo $error_message;?>" name="EMH" type="text" >
                                        </div>
			                           <div class="form-group">
                                            <label>Didn't Receive OTP? </label>
                                           <input class="form-control regcom" value="<?php echo $OTP;?>" name="DRO" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Re-Send </label>
                                           <input class="form-control regcom" value="<?php echo $resends;?>" name="resend" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Please check your email for verification code</label>
                                           <input class="form-control regcom" value="<?php echo $re_code;?>" name="PCEV" type="text" >
                                        </div>
										
										<div class="form-group">
                                            <label>Password</label>
                                           <input class="form-control regcom" value="<?php echo $passwords;?>" name="Password" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Hide</label>
                                           <input class="form-control regcom" value="<?php echo $hide;?>" name="Hide" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Remember me</label>
                                           <input class="form-control regcom" value="<?php echo $remember_me;?>" name="remenber" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Forgot your password?</label>
                                           <input class="form-control regcom" value="<?php echo $forgot_pswd;?>" name="FYP" type="text" >
                                        </div>
										
										<div class="form-group">
                                            <label>Save This Address</label>
                                           <input class="form-control regcom" value="<?php echo $save_address;?>" name="save_addr" type="text" >
                                        </div>
										<div class="form-group">
                                            <label>Pay Now</label>
                                           <input class="form-control regcom" placeholder="Pay Now" name="PayNow" type="text" value="<?php echo $PayNow;?>" >
                                        </div>
										<div class="form-group">
                                            <label>Book Now</label>
                                           <input class="form-control regcom" placeholder="Book Now" name="BookNow" type="text"  value="<?php echo $BookNow;?>">
                                        </div>
										<div class="form-group">
                                            <label>Find my Taxi</label>
                                           <input class="form-control regcom" placeholder="Find my Taxi" name="FindmyTaxi" type="text"  value="<?php echo $FindmyTaxi;?>">
                                        </div>
										</form> 
	      </div>
	  </div>
	  
	  
	  
	  
	   </div>
				  
	</div>			   
			
      </div><!-- /.content-wrapper -->
     <?php
	 include"includes/admin-footer.php";
	 ?>

      <!-- Control Sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
   <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
	
	 
    <script src="<?php echo base_url();?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/adminlte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/adminlte/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>assets/adminlte/dist/js/demo.js"></script>
	
	 <script src="<?php echo base_url();?>assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.js"></script>
     <script src="<?php echo base_url();?>assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- page script -->
	  <script src="<?php echo base_url();?>assets/adminlte/dist/js/sb-admin-2.js"></script>
    <!-- page script -->
   <script src="<?php echo base_url();?>assets/adminlte/plugins/select2/select2.full.min.js"></script>
   
   
   
     
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
        <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    
  
    
       <script type="text/javascript">
$(document).ready(function(){
$(".sample").on("keydown", function (e) {
        return e.which !== 32;
	    });  
	
        $('.sample').keyup(function()
	     {
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
			var isSplChar = re.test(yourInput);
			if(isSplChar)
			{
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				$(this).val(no_spl_char);
			}
		});	
					   
$('.regcom').on('change', function (){
var a = $(this).val();
if(a != '') {
$(this).removeClass('error-admin');

} else {
$(this).addClass('error-admin');
return false;
}
 });						   
						   
						   
 $("#useredit").click(function(e){

  var isValid = true;
        $('input[type="text"]').each(function() {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    
                });
				$(this).focus();
				return false;
            }
            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
		 if (isValid == false) {
            e.preventDefault();
		 }
        else {
            
	          var value =$("#taxi_reg").serialize() ;

$.ajax({
url:'<?php echo base_url();?>admin/insert_language',
method:'post',
data:value,
success:function(res){
$(".edituser").show();
console.log(res);
if(res==0){
	 $(".edituser").focus();
	$(".edituser").html('<p class="error">Error</p>');
	setTimeout(function(){$(".edituser").hide(); }, 3000);
	
}else if(res==2){
	
	$(".edituser").focus();
	$(".edituser").html('<p class="error">Language Exists</p>');
	setTimeout(function(){$(".edituser").hide(); }, 3000);
}
else{
$(".edituser").focus();
$(".edituser").html('<p class="success">Add Language Saved Successfully</p>');
setTimeout(function(){$(".edituser").hide(); }, 1500);
$('#taxi_reg')[0].reset();
}
}
}); 
		}
	            
                                       
 
});
});

</script>

  </body>
</html>
